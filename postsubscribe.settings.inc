<?php
/*
 *  @file
 *  Copyright (C) The World Bank. 2008. http://www.worldbank.org
 *  Author: Lasha Dolidze
 *
 */



function postsubscribe_form_alter(&$form, $form_state, $form_id) {

 if (user_access('allow send node to subscribers')) {
    $ct_array = array_filter(array_values((array)variable_get('postsubscribe_nodetypes', '')));
   
    if (in_array($form['type']['#value'], $ct_array)) {
      $checked = array('checked' => 'checked');  
    }
    
    if ($form['#id'] != 'node-form') {
      return;
    }
    
    $form['postsubscribe'] = array(
      '#type' => 'fieldset',
      '#access' => 1,
      '#title' => t('Postsubscribe'),
      '#collapsible' => 1,
      '#collapsed' => 0,
      '#weight' => 26,
    );
  
    $form['postsubscribe']['send_to_subscribers'] = array(
      '#type' => 'checkbox',
      '#title' => t('Send node to subscribers'),
      '#default_value' => variable_get('send_to_subscribers', 0),
      '#attributes' => $checked,
    );
  
    return $form;
  }
}




function postsubscribe_settings() {

  $address_default = variable_get('site_mail', ini_get('sendmail_from'));
  $name_default = variable_get('site_name', 'drupal');
  
  // general settings.
  $form['email_general'] = array(
    '#type' => 'fieldset', 
    '#title' => t('General settings'),
    '#collapsible' => TRUE, 
    '#collapsed' => FALSE,
    );

  $form['email_general']['postsubscribe_nodetypes'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Send e-mail on this node types'),
    '#description' => t('Send e-mail on this node types'),
    '#default_value' => variable_get('postsubscribe_nodetypes', array('blog')),
    '#options' => node_get_types('names'),  
  );
  
  $form['email_general']['send_email_immediately'] = array('#type' => 'select',
    '#title' => t('Send e-mail settings'),
    '#description' => t('Send e-mail immediately when new content added. Dont use in production server, but this is good option for testing purposes.
     Send e-mail daily(end of the day sends combination of posts) when new content added.Please note - cron must be run'),
    '#default_value' => variable_get('send_email_immediately', FALSE),
    '#options' => array(0 => t('Send e-mail immediately'), 1 => t('Send e-mail daily')),    
  );

  $hours = array( 0 => 'None', 17 => '5:00 PM',
                 18 => '6:00 PM', 19 => '7:00 PM', 
                 20 => '8:00 PM', 21 => '9:00 PM', 
                 22 => '10:00 PM', 23 => '11:00 PM', 24 => '12:00 PM'); 
  $form['email_general']['postsubscribe_cron_time'] = array(
     '#type' => 'select',
     '#title' => t('Send time'),
     '#options' => $hours,
     '#default_value' => variable_get('postsubscribe_cron_time', 0),
     '#description' => t('Digest daily Sending time'),
  );

  $form['email_general']['postsubscribe_from_address'] = array(
    '#type' => 'textfield',
    '#title' => t('From address'),
    '#description' => t('The address to send from'),
    '#default_value' => variable_get('postsubscribe_from_address', $address_default),
  );
  
  $form['email_general']['postsubscribe_from_name'] = array(
    '#type' => 'textfield',
    '#title' => t('From name'),
    '#description' => t('The name to send from'),
    '#default_value' => variable_get('postsubscribe_from_name', $name_default),
  );
  
  $form['email_general']['postsubscribe_header_image'] = array(
    '#type' => 'textfield',
    '#title' => t('Header image'),
    '#description' => t('Enter the URL of the image to as a logo at the top of e-mail message.'),
    '#default_value' => variable_get('postsubscribe_header_image', ""),
  );
  
  
  // Traffic tracking settings
  $form['postsubscribe_traffic_tracking_settings'] = array(
        '#type' => 'fieldset', 
        '#title' => t('Traffic tracking settings'),
        '#collapsible' => TRUE, 
        '#collapsed' => FALSE,
  );
  $form['postsubscribe_traffic_tracking_settings']['postsubscribe_domain_names'] = array(
    '#type' => 'textarea',
    '#title' => t('Domain names'),
    '#default_value' => variable_get('postsubscribe_domain_names', ''),
    '#description' => t('List of domains to append a traffic parameter. Separate multiple domains with a comma.'),
    '#cols' => 20,
    '#rows' => 10,
     );
  $form['postsubscribe_traffic_tracking_settings']['postsubscribe_traffick_parameter_to_append'] = array(
    '#type' => 'textfield',
    '#title' => t('Traffic parameter'),
    '#description' => t('Traffic parameter to append key=value pair, for example: cid=AFR_africacanealert_P_EXT'),
    '#default_value' => variable_get('postsubscribe_traffick_parameter_to_append', ''),
  );
      
  
  // e-mail notification settings.
  $form['email_notification'] = array(
        '#type' => 'fieldset', 
        '#title' => t('E-mail notification settings'),
        '#collapsible' => TRUE, 
        '#collapsed' => FALSE,
  );

     
  $form['email_notification']['postsubscribe_mail_notification_subject'] = array(
        '#type' => 'textfield',
        '#title' => t('Subject of notification e-mail'),
        '#default_value' => _postsubscribe_mail_text('notification_subject'),
        '#maxlength' => 180,
        '#description' => t('Customize the subject of the notification e-mail for subscribed member. The following placeholders are available:
                          <dl>
                            <dt>!site</dt>
                            <dd>The name of your site.</dd>
                            
                            <dt>!email</dt>
                            <dd>The e-mail address of the subscriber.</dd>
                            
                            <dt>!content_title</dt>
                            <dd>The title of the submited content.</dd>
                            
                            <dt>!content_url</dt>
                            <dd>The address of the submited content.</dd>
                          </dl>'),
  );
  
  $form['email_notification']['postsubscribe_mail_notification_body'] = array(
        '#type' => 'textarea',
        '#title' => t('Body of notification e-mail'),
        '#default_value' => _postsubscribe_mail_text('notification_body'),
        '#rows' => 15,
        '#description' => t('Customize the body of the notification e-mail for subscribed member. The following placeholders are available:
                          <dl>
                            <dt>!site</dt>
                            <dd>The name of your site.</dd>
                            
                            <dt>!email</dt>
                            <dd>The e-mail address of the subscriber.</dd>
                            
                            <dt>!content_title</dt>
                            <dd>The title of the submited content.</dd>

                            <dt>!content_teaser</dt>
                            <dd>The teaser of the submited content.</dd>

                            <dt>!content_body</dt>
                            <dd>The body of the submited content.</dd>
                                                        
                            <dt>!content_url</dt>
                            <dd>The address of the submited content.</dd>
                            
                            <dt>!content_add_comment</dt>
                            <dd>The address the subscriber can visit to submit the comment.</dd>

                            <dt>!content_author_name</dt>
                            <dd>The name of the user who submitted the content..</dd>
                            
                            <dt>!content_date</dt>
                            <dd>The creation date of the submited content.</dd>

                            <dt>!content_title_url</dt>
                            <dd>The address of the submited content.</dd>

                            <dt>!unsubscribe_url</dt>
                            <dd>The address of the unsubscribe page.</dd>
                            
                          </dl>'),
  );


  // e-mail confirmation settings.
  $form['email'] = array(
        '#type' => 'fieldset', 
        '#title' => t('E-mail confirmation settings'),
        '#collapsible' => TRUE, 
        '#collapsed' => FALSE,
  );
  
  $form['email']['postsubscribe_mail_subscribe_subject'] = array(
        '#type' => 'textfield',
        '#title' => t('Subject of subscribe e-mail'),
        '#default_value' => _postsubscribe_mail_text('subscribe_subject'),
        '#maxlength' => 180,
        '#description' => t('Customize the subject of your subscribe welcome e-mail, which is sent to new member upon request. The following placeholders are available:
                          <dl>
                            <dt>!site</dt>
                            <dd>The name of your site.</dd>
                            
                            <dt>!email</dt>
                            <dd>The e-mail address of the subscriber.</dd>
                            
                            <dt>!site_url</dt>
                            <dd>The address of your site.</dd>

                          </dl>'),
  );
        
  
  $form['email']['postsubscribe_mail_subscribe_body'] = array(
        '#type' => 'textarea',
        '#title' => t('Body of subscribe e-mail (for new member)'),
        '#default_value' => _postsubscribe_mail_text('subscribe_body'),
        '#rows' => 15,
        '#description' => t('Customize the body of the subscribe e-mail for subscribed member, which is sent to new member upon request. The following placeholders are available:
                          <dl>
                            <dt>!site</dt>
                            <dd>The name of your site.</dd>
                            
                            <dt>!email</dt>
                            <dd>The e-mail address of the subscriber.</dd>
                            
                            <dt>!site_url</dt>
                            <dd>The address of your site.</dd>

                            <dt>!subscribe_url</dt>
                            <dd>The address of the subscribe confirmation page.</dd>

                          </dl>'),
  );

  
  $form['email']['postsubscribe_mail_subscribe_is_user_body'] = array(
        '#type' => 'textarea',
        '#title' => t('Body of subscribe e-mail (for alredy subscribed member)'),
        '#default_value' => _postsubscribe_mail_text('subscribe_is_user_body'),
        '#rows' => 15, '#description' => t('Customize the body of the subscribe e-mail for already subscribed member, which is sent to new member upon request. The following placeholders are available:
                          <dl>
                            <dt>!site</dt>
                            <dd>The name of your site.</dd>
                            
                            <dt>!email</dt>
                            <dd>The e-mail address of the subscriber.</dd>
                            
                            <dt>!site_url</dt>
                            <dd>The address of your site.</dd>

                          </dl>'),
  );

  $form['email']['postsubscribe_mail_unsubscribe_body'] = array(
        '#type' => 'textarea',
        '#title' => t('Body of unsubscribe e-mail (for not subscribed member)'),
        '#default_value' => _postsubscribe_mail_text('unsubscribe_body'),
        '#rows' => 15,
        '#description' => t('Customize the body of the unsubscribe e-mail for subscribed member, which is sent to  member upon request. The following placeholders are available:
                          <dl>
                            <dt>!site</dt>
                            <dd>The name of your site.</dd>
                            
                            <dt>!email</dt>
                            <dd>The e-mail address of the subscriber.</dd>
                            
                            <dt>!site_url</dt>
                            <dd>The address of your site.</dd>

                          </dl>'),
  );

  
  $form['email']['postsubscribe_mail_unsubscribe_is_user_body'] = array(
        '#type' => 'textarea',
        '#title' => t('Body of unsubscribe e-mail (for alredy subscribed member)'),
        '#default_value' => _postsubscribe_mail_text('unsubscribe_is_user_body'),
        '#rows' => 15,
        '#description' => t('Customize the body of the unsubscribe e-mail for already subscribed member, which is sent to member upon request. The following placeholders are available:
                          <dl>
                            <dt>!site</dt>
                            <dd>The name of your site.</dd>
                            
                            <dt>!email</dt>
                            <dd>The e-mail address of the subscriber.</dd>
                            
                            <dt>!site_url</dt>
                            <dd>The address of your site.</dd>

                            <dt>!unsubscribe_url</dt>
                            <dd>The address of the unsibscribe confirmation page.</dd>

                          </dl>'),
  );
  
  $form['#submit'][] = 'postsubscribe_settings_form_submit';
  
  return system_settings_form($form);
}


function postsubscribe_settings_form_submit($form, &$form_state) {

  $op = isset($form_state['values']['op']) ? $form_state['values']['op'] : '';

  // Exclude unnecessary elements.
  unset($form_state['values']['submit'], $form_state['values']['reset'], $form_state['values']['form_id'], $form_state['values']['op'],     $form_state['values']['form_token'], $form_state['values']['form_build_id']);

  foreach ($form_state['values'] as $key => $value) {
    if ($op == t('Reset to defaults')) {
      
      if (strpos($key, 'postsubscribe_mail_') === 0) {
        postsubscribe_variable_del($key);
      }
      else {
        variable_del($key);      
      }      
      
    }
    else {
      if (is_array($value) && isset($form_state['values']['array_filter'])) {
        $value = array_keys(array_filter($value));
      }
      
      if (strpos($key, 'postsubscribe_mail_') === 0) {
        postsubscribe_variable_set($key, $value);
      }
      else {
        variable_set($key, $value);     
      } 
      
    }
  }
  if ($op == t('Reset to defaults')) {
    drupal_set_message(t('The configuration options have been reset to their default values.'));
  }
  else {
    drupal_set_message(t('The configuration options have been saved.'));
  }

  cache_clear_all();
  drupal_rebuild_theme_registry();
}


function postsubscribe_variable_set($name, $value) {
  global $postsubscribe_conf;

  $serialized_value = serialize($value);
  db_query("UPDATE {post_subscriptions_variable} SET value = '%s' WHERE name = '%s'", $serialized_value, $name);
  if (!db_affected_rows()) {
    @db_query("INSERT INTO {post_subscriptions_variable} (name, value) VALUES ('%s', '%s')", $name, $serialized_value);
  }

  cache_clear_all('postsubscribe_variables', 'cache');

  $postsubscribe_conf[$name] = $value;
}


function postsubscribe_variable_del($name) {
  global $postsubscribe_conf;

  db_query("DELETE FROM {post_subscriptions_variable} WHERE name = '%s'", $name);
  cache_clear_all('postsubscribe_variables', 'cache');

  unset($postsubscribe_conf[$name]);
}


function postsubscribe_variable_get($name, $default) {
  global $postsubscribe_conf;

  return isset($postsubscribe_conf[$name]) ? $postsubscribe_conf[$name] : $default;
}

/*
* Default mail tempates 
*/
function _postsubscribe_mail_text($messageid, $variables = array()) {
 
  // Check if an postsubscribe setting overrides the default string.
  if ($postsubscribe_setting = postsubscribe_variable_get('postsubscribe_mail_'. $messageid, FALSE)) {
    return strtr($postsubscribe_setting, $variables);
  }
  
    $body = "";
    switch ($messageid) {
      case 'subscribe_subject':
        return t('Confirmation for e-mail notofication from !site', $variables);
      case 'subscribe_is_user_body':
        return t("This is a subscription status confirmation notice\n\nWe have received a request for subscription of your e-mail address, !email, to the !site (!site_url).\nHowever, you are already subscribed. If you want to unsubscribe, you can visit our website by using the link at the bottom of this e-mail.\n\n----\nVisit our site: !site_url", $variables);
      case 'subscribe_body':
         $body .= t("This is a subscription status confirmation notice\n\nWe have received a request for subscription of your e-mail address, !email, to the !site (!site_url).\nTo confirm that you want to be added to this mailing list, simply visit the confirmation link at the bottom of this e-mail.", $variables);
         $body .= "\n\n". t('If you do not wish to be subscribed to this site, please disregard this message.');
         $body .= "\n\n-- \n". t('Subscribe link: !subscribe_url', $variables);
         return $body;

      case 'unsubscribe_is_user_body':
        $body .= t("We have received a request for the removal of your e-mail address, !email, from !site (!site_url).\nIf you want to unsubscribe, simply visit the confirmation link at the bottom of this e-mail.", $variables);
        $body .= "\n\n". t("If you did not make this request, please disregard this message.");
        $body .= "\n\n-- \n". t("Unsubscribe link: !unsubscribe_url", $variables);
        return $body;
      case 'unsubscribe_body':
         $body .= t("We have received a request for the removal of your e-mail address, !email, from !site (!site_url).\nHowever, you were not subscribed. If you want to subscribe, you can visit our website by using the link at the bottom of this e-mail.", $variables);
         $body .= "\n\n". t("If you do not wish to be subscribed to this site, please disregard this message.");
         $body .= "\n\n----\n". t("Visit our site: !site_url");
         return $body;
         
      case 'notification_subject':
         return t('!content_title', $variables);
      case 'notification_body':
         $body .= t("!content_title\n\n<br/><br/>!content_body\n\n<br/><br/><br/>Click here !content_url to view full story.\n\n<br/><br/>You're receiving this e-mail because you subscribed to !site. If you wish to unsubscribe from this notification, please using the link at the bottom of this e-mail.", $variables);
         $body .= "\n\n<br/><br/>-- \n<br/>". t("Unsubscribe link: !unsubscribe_url", $variables);
         return $body;   
    }
}
