DESCRIPTION
-----------
Postsubscribe 

The Postsubscribe module give the visitors to your site the ability to 
sign up for email updates whenever new content is posted. It also handles 
comment settings, allowing site administrators to receive email when a new 
comment has been submitted. Administrative settings are available at 
Site Configuration - Postsubscribe settings. 

Email update settings 

When Postsubscribe is installed, a "send node to subscriber" option is added 
to all content near the bottom of the content entry form. The main Postsubscribe 
settings screen allows you to choose which content types should have this option 
checked by default. However, this option can be toggled on or off as each content 
item is published. For example, even if the box is checked by default, it can be 
unchecked if you would prefer a particular item not be sent to subscribers, or if 
you're making a minor edit to an existing item and would prefer another email not 
be sent. The settings page also has options for when to send, the from address, 
default text in the emails that are sent, and other similar settings. 

Postsubsribe creates a block, "Post subscribe", which can be added to a sidebar or
other region in your theme. The block has an entry form for site visitors to enter
their email address. When a visitor enters their address, a confirmation email is 
sent, and they will only receive email updates after they click a confirmation 
link in that email. 

The "Subscribers" tab in the Postsubscribe settings allows you to see who has signed
up and to manage subscribers. There are options to subscribe/unsubscribe and to
sort, import and export email addresses. 

Comment settings 

The Comment settings tab allows you to set who should receive email alerts when new 
comments are posted. It includes options to send email whenever a new comment is submitted
or only when comments needing approval are submitted.

Sending of large mailings can be managed by cron.

REQUIREMENT
-----------
 * Drupal 6
 * For large mailing lists, cron is required.
 

INSTALLATION
------------

 1. CREATE DIRECTORY

    Create a new directory "postsubscribe" in the sites/all/modules directory and
    place the entire contents of this "postsubscribe" folder in it.

 2. ENABLE THE MODULE

    Enable the module on the Modules admin page:
      Administer > Site building > Modules

 3. ACCESS PERMISSION

    Grant the proper access to user accounts at the Access control page:
      Administer > User management > Access control. 
    To enable users to (un)subscribe to a new content use the "subscribe to
    new post" permission. This will enable the Postsubscribe block where the
    user can (un)subscribe. 

 4. ENABLE POSTSUBSCRIBE BLOCK

    Enable the Postsubscribe block on the Administer blocks page:
      Administer > Site building > Blocks.
    One block "Post subscribe" is available, simple enable it.

 5. CONFIGURE POSTSUBSCRIBE

    Configure Postsubscribe on the Postsubscribe admin pages:
      Administer > Site Configuration > Postsubscribe Settings.

 6. CONFIGURE POSTSUBSCRIBE BLOCK

    Configure the Postsubscribe block on the Block configuration page. You reach
    this page from Block admin page (Administer > Site building > Blocks). Click
    the 'Configure' link of the appropriate "Post subscribe" block.
 
    Permission "subscribe to new post" is required to view the subscription
    form in the "Post subscribe" block.


SEND NOTIFICATION EMAIL TWO OPTIONS
-----------------------------------

Cron jobs are required to send large mailing lists. Cron jobs can be triggered
by crontab or any other cron mechanisme. If you have a medium or large size 
mailinglist (i.e. more than 500 subscribers) always use "Send e-mail daily" 
from postsubscribe administration page to send the email notification.
  
When you use "Send e-mail daily" setting:
 * Set the Digest daily sending time to appropriate value.
   Failure may lead to sending duplicate email notification.
 * Make sure the number of email notification send per cron run (Cron throttle) is not
   too high. Too high values will lead to the warning message 'Attempting to re-run cron
   while it is already running'
    
When you use "Send e-mail immediately" setting:
 * This option sends e-mail immediately when new content added. 
   Don't use in production server, but this is good option for testing purposes. 
    


