<?php
/*
 *  @file
 *  Copyright (C) The World Bank. 2008. http://www.worldbank.org
 *	Author: Lasha Dolidze
 *
 */

define('COMMENTMAIL_DEFAULT_APPROVE', t("An unapproved comment has been posted on !site for the post '!node'. You need to publish this comment before it will appear on your site.

approve/edit/delete/ban: !quick_approve

Name: !user  | Email: !mail  | URL: !homepage  | IP: !host
Comment:

!subject

!comment

approve: !approval_url
edit: !edit_url
delete: !delete_url
ban: !ban_url

Comment administration: !admin_url"));

define('COMMENTMAIL_DEFAULT_NOTIFY', t("A new comment has been posted on !site for the post '!node'.

THIS COMMENT DOES NOT REQUIRE APPROVAL

Name: !user  | Email: !mail  | URL: !homepage  | IP: !host
Comment:

!subject

!comment

approve: !approval_url
edit: !edit_url
delete: !delete_url
ban: !ban_url

Comment administration: !admin_url"));


function postsubscribe_commentmail_settings() {
$form['postsubscribe_commentmail_to'] = array(
    '#type' => 'textarea',
    '#title' => t('Send to'),
    '#default_value' => variable_get('postsubscribe_commentmail_to', variable_get('site_mail', '')),
    '#description' => t('A mail message will be sent here when new comments are posted to the site. Separate multiple addresses with a comma.'),
    '#cols' => 20,
    '#rows' => 10,
     );

  $form['postsubscribe_commentmail_mode'] = array(
    '#type' => 'radios',
    '#title' => t('Send mail for'),
    '#default_value' => variable_get('postsubscribe_commentmail_mode', 'approval'),
    '#options' => array(
      'all' => t('all new comments'),
      'approval' => t('just comments needing approval'),
      'disable' => t('none (disabled)'),
    ),
  );

  $placeholders = t("The following placeholders are available:
<dl>
  <dt>!site</dt>
  <dd>The name of your site.</dd>

  <dt>!node</dt>
  <dd>The title of the post this comment was submitted for.</dd>

  <dt>!approval_url</dt>
  <dd>The address the recipient can visit to publish the comment.</dd>

  <dt>!delete_url</dt>
  <dd>The address the recipient can visit to delete the comment.</dd>

  <dt>!ban_url</dt>
  <dd>The address the recipient can visit to delete the comment and ban the user who submitted the comment.</dd>

  <dt>!edit_url</dt>
  <dd>The address the recipient can visit to edit the comment.</dd>

  <dt>!view_url</dt>
  <dd>The address the recipient can visit to view the comment.</dd>

  <dt>!admin_url</dt>
  <dd>The address of the comment moderation.</dd>

  <dt>!queue_url</dt>
  <dd>The address of the comment moderation approval queue.</dd>

  <dt>!host</dt>
  <dd>The host name of the user who submitted the comment.</dd>

  <dt>!user</dt>
  <dd>The name of the user who submitted the comment.</dd>

  <dt>!mail</dt>
  <dd>The e-mail address of the user who submitted the comment.</dd>

  <dt>!homepage</dt>
  <dd>The homepage of the user who submitted the comment.</dd>

  <dt>!subject</dt>
  <dd>The comment's subject line.</dd>

  <dt>!comment</dt>
  <dd>The actual comment text the user submitted.</dd>");

  $form['approval_mails'] = array(
        '#type' => 'fieldset', 
        '#title' => t('Approval emails'),
        '#collapsible' => TRUE, 
        '#collapsed' => TRUE,
  );
  
  $form['approval_mails']['postsubscribe_commentmail_mail_approve'] = array(
    '#type' => 'textarea',
    '#title' => t('Body text for approval mails'),
    '#default_value' => variable_get('postsubscribe_commentmail_mail_approve', t(COMMENTMAIL_DEFAULT_APPROVE)),
    '#description' => $placeholders,
    '#cols' => 60,
    '#rows' => 25,
  );

  $form['notification_mails'] = array(
        '#type' => 'fieldset', 
        '#title' => t('Notification emails'),
        '#collapsible' => TRUE, 
        '#collapsed' => TRUE,
  );
  
  $form['notification_mails']['postsubscribe_commentmail_mail_notify'] = array(
    '#type' => 'textarea',
    '#title' => t('Body text for notification mails'),
    '#default_value' => variable_get('postsubscribe_commentmail_mail_notify', t(COMMENTMAIL_DEFAULT_NOTIFY)),
    '#description' => $placeholders,
    '#cols' => 60,
    '#rows' => 25,
  );

  return system_settings_form($form);
  
}



/**
 * Implementation of hook_comment() see postsubscribe.module.
 */
function postsubscribe_commentmail_comment_wrapper($comment, $op) {
 
  global $language;

  if ($op == 'insert') {
    // Load the real comment object from the database.
    $comment_obj = _comment_load($comment['cid']);

    // Decide what to do based on the user's setting.
    switch (variable_get('postsubscribe_commentmail_mode', 'approval')) {
      // Mails should only be sent for unpublished comments.
      case 'approval':
        if ($comment_obj->status == 0) {
          // Don't send a mail because the comment is already published.
          break;
        }
      // Fallthrough for unpublished comments.
      // Mail should be sent for all new comments, regardless of their status.
      case 'all':
        $recipient = variable_get('postsubscribe_commentmail_to', variable_get('site_mail', FALSE));

        // Only send a mail if a recipient has been specified.
        if ($recipient) {
          // Load the node to get the title.
          $node = node_load($comment_obj->nid);

          if ($comment_obj->status == COMMENT_NOT_PUBLISHED) {
            $text = variable_get('postsubscribe_commentmail_mail_approve', t(COMMENTMAIL_DEFAULT_APPROVE));
          }
          else {
            $text = variable_get('postsubscribe_commentmail_mail_notify', t(COMMENTMAIL_DEFAULT_NOTIFY));
          }

          // Check if the user is logged in.
          if ($comment_obj->uid) {
            $account = user_load(array('uid' => $comment_obj->uid));
            $comment_obj->mail = $account->mail;
            $comment_obj->homepage = $account->homepage;
          }

          $body = strtr($text, array(
            '!site' => variable_get('site_name', 'Drupal'),
            '!node' => $node->title,
            '!approval_url' => url('comment/approve/'. $comment_obj->cid, array('query' => NULL, 'fragment' => NULL, 'absolute' => TRUE)),
            '!delete_url' => url('comment/delete/'. $comment_obj->cid, array('query' => NULL, 'fragment' => NULL, 'absolute' => TRUE)),
            '!ban_url' => url('comment/deleteban/'. $comment_obj->cid, array('query' => NULL, 'fragment' => NULL, 'absolute' => TRUE)),
            '!edit_url' => url('comment/edit/'. $comment_obj->cid, array('query' => NULL, 'fragment' => NULL, 'absolute' => TRUE)),
            '!queue_url' => url('admin/content/comment/list/approval', array('query' => NULL, 'fragment' => NULL, 'absolute' => TRUE)),
            '!view_url' => url('node/'. $node->nid, array('query' => NULL, 'fragment' => 'comment-'. $comment_obj->cid, 'absolute' => TRUE)),
            '!admin_url' => url('admin/content/comment', array('query' => NULL, 'fragment' => NULL, 'absolute' => TRUE)),
            '!host' => $comment_obj->hostname,
            '!user' => $comment_obj->name,
            '!mail' => $comment_obj->mail,
            '!homepage' => $comment_obj->homepage,
            '!subject' => $comment_obj->subject,
            '!comment' => $comment_obj->comment,
          ));

  $subject = t('[!site] New Comment posted on "!title"', array('!site' => variable_get('site_name', 'Drupal'), '!title' => $node->title)); 
  drupal_mail('postsubscribe', 'send_mail', $recipient, $language->language, array('subject' => $subject, 'body' => $body), variable_get('site_mail', NULL), TRUE);

        }
        else {
          watchdog('commentmail', 'Site mail address is not configured.', WATCHDOG_ERROR);
        }
    }
  }
}



/**
 * Approve a comment.
 *
 * @param $cid
 *   Comment ID of comment to be approved.
 */
function postsubscribe_commentmail_approve(&$form_state, $cid) {
  if ($comment = _comment_load($cid)) {
    if ($comment->status == COMMENT_NOT_PUBLISHED) {
      return confirm_form(
        array('cid' => array('#type' => 'value', '#value' => $comment->cid)),
        t('Are you sure you want to approve the comment %title?', array('%title' => $comment->subject)),
        array('path' => 'node/'. $comment->nid, 'fragment' => 'comment-'. $comment->cid),
        t('The comment will be visible to all users.'),
        t('Approve'),
        t('Cancel')
      );
    }
    else {
      drupal_set_message(t('The comment is already published.'));
      drupal_goto('node/'. $comment->nid, NULL, 'comment-'. $comment->cid);
    }
  }
  else {
    drupal_set_message(t('The comment no longer exists.'));
  }
}

/**
 * Submission handler for comment approval.
 */
function postsubscribe_commentmail_approve_submit($form, &$form_state) {
  $comment = _comment_load($form_state['values']['cid']);
  $comment->status = 0;

  if (comment_save((array)$comment)) {
    // Link to comment on page.
    drupal_set_message(t('The comment has been approved.'));
    drupal_goto('node/'. $comment->nid, NULL, 'comment-'. $comment->cid);
  }
  else {
    drupal_set_message('There was an error during the comment approving process.', 'error');
  }
}

/**
 * Delete a comment and ban the author.
 *
 * @param $cid
 *   Comment ID of comment to be deleted.
 */
function postsubscribe_commentmail_deleteban(&$form_state, $cid) {
  if ($comment = _comment_load($cid)) {
    return confirm_form(
        array('cid' => array('#type' => 'value', '#value' => $comment->cid)),
        t('Are you sure you want to delete the comment %title and ban its author?', array('%title' => $comment->subject)),
        array('path' => 'node/'. $comment->nid, 'fragment' => 'comment-'. $comment->cid),
        t('Any replies to this comment will be lost. This action cannot be undone. In addition, the author of the comment is no longer allowed to post comments on your site.'),
        t('Delete and ban'),
        t('Cancel')
      );
  }
  else {
    drupal_set_message(t('The comment no longer exists.'));
  }
}

/**
 * Submission handler for comment delete/ban.
 */
function postsubscribe_commentmail_deleteban_submit($form, &$form_state) {

  $mpath = drupal_get_path('module', 'comment');
  require_once( $mpath ."/comment.admin.inc");
    
  $comment = _comment_load($form_state['values']['cid']);

  // Delete comment and its replies.
  _comment_delete_thread($comment);
  _comment_update_node_statistics($comment->nid);

  drupal_set_message(t('The comment and all its replies have been deleted.'));

  // Clear the cache so an anonymous user sees that his comment was deleted.
  cache_clear_all();

  // Now, ban the user.  
  $ip = $comment->hostname;
  db_query("INSERT INTO {access} (mask, type, status) VALUES ('%s', '%s', %d)", $ip, 'host', 0);
  watchdog('action', 'Banned IP address %ip', array('%ip' => $ip));
   
  drupal_set_message(t('The host %host has been banned.', array('%host' => $comment->hostname)));

  drupal_goto('node/'. $comment->nid);
}
