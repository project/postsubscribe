<?php
/*
 *  @file
 *  Copyright (C) The World Bank. 2008. http://www.worldbank.org
 *	Author: Lasha Dolidze
 *
 */


function postsubscribe_subscribers($callback_arg = '') {

  if ($_POST['accounts'] && $_POST['operation'] == 'delete') {
    $output = drupal_get_form('postsubscribe_subscribers_multiple_delete_confirm');
  }
  else {
    $output = drupal_get_form('postsubscribe_subscribers_filter_form');
    $output .= drupal_get_form('postsubscribe_subscribers_account');
  }
  
  $subscr = db_result(db_query('SELECT count(*) as subscr FROM {post_subscriptions} WHERE status = 2'));
  $unsubsc = db_result(db_query('SELECT count(*) as unsubsc FROM {post_subscriptions} WHERE status = 1'));
  $totalsubscribers = $subscr + $unsubsc;
  
  $output .= "
  <ul>
    <li>Subscribed $subscr</li>
    <li>Unsubscribed $unsubsc</li>
    <li>Total $totalsubscribers</li>    
  </ul>";
  
  return $output;
}




/**
 * Build query for subscribers filters based on session.
 */
function postsubscribe_subscribers_build_filter_query() {
  $filters = postsubscribe_subscribers_filters();
  
  // Build query
  $where = $args = $join = array();
  foreach ($_SESSION['postsubscribe_subscribers_overview_filter'] as $filter) {
    list($key, $value) = $filter;
    $where[] = $filters[$key]['where'];
    $args[] = $value;
    $join[] = $filters[$key]['join'];
  }
  $where = count($where) ? 'AND '. implode(' AND ', $where) : '';
  $join = count($join) ? ' '. implode(' ', array_unique($join)) : '';

  return array('where' => $where,
           'join' => $join,
           'args' => $args,
         );
}

/**
 * Theme subscribers overview.
 */
function theme_postsubscribe_subscribers_account($form) {
  // Overview table:
  $header = array(
    theme('table_select_header_cell'),
    array('data' => t('E-mail'), 'field' => 'u.name'),
    array('data' => t('Status'), 'field' => 'u.status')
  );

  $output = drupal_render($form['operations']);
  if (isset($form['mail']) && is_array($form['mail'])) {
    foreach (element_children($form['mail']) as $key) {
      $rows[] = array(
        drupal_render($form['accounts'][$key]),
        drupal_render($form['mail'][$key]),
        drupal_render($form['status'][$key]),
      );
    }
  }
  else  {
    $rows[] = array(array('data' => t('No subscribers available.'), 'colspan' => '7'));
  }

  $output .= theme('table', $header, $rows);
  if ($form['pager']['#value']) {
    $output .= drupal_render($form['pager']);
  }

  $output .= drupal_render($form);

  return $output;
}

function postsubscribe_subscribers_account() {
  $filter = postsubscribe_subscribers_build_filter_query();

  $header = array(
    array(),
    array('data' => t('Email'), 'field' => 's.mail'),
    array('data' => t('Status'), 'field' => 's.status')
  );

  $sql = 'SELECT DISTINCT s.snid, s.mail, s.status FROM {post_subscriptions} s where 1=1 '. $filter['where'];
  $sql .= tablesort_sql($header);
  $query_count = 'SELECT COUNT(DISTINCT s.snid) FROM {post_subscriptions} s where 1=1 '. $filter['where'];
  $result = pager_query($sql, 50, 0, $query_count, $filter['args']);

  $form['operations'] = array(
    '#type' => 'fieldset',
    '#title' => t('Subscriber operations'),
    '#collapsible' => TRUE, 
    '#collapsed' => FALSE,
  );

  // START --------------------------------- Update options
  $form['operations']['options'] = array(
    '#type' => 'fieldset',
    '#title' => t('Update options'),
    '#prefix' => '<div class="container-inline">',
    '#suffix' => '</div>',
  );
  
  $options = array();
 
  $operations = postsubscribe_subscribers_operations();
  
  foreach ($operations as $operation => $array) {
    $options[$operation] = $array['label'];
  }
  
  $form['operations']['options']['operation'] = array(
    '#type' => 'select',
    '#options' => $options,
    '#default_value' => 'unblock',
  );

  $form['operations']['options']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Update'),
  );
  // END --------------------------------- Update options

  // START --------------------------------- Add subscriber 
  $form['operations']['subscriber'] = array(
    '#type' => 'fieldset',
    '#title' => t('Add subscriber'),
    '#prefix' => '<div class="container-inline">',
    '#suffix' => '</div>',
  );

  $form['operations']['subscriber']['subscriber'] = array(
    '#type' => 'textfield',
    '#default_value' => '',
    '#size' => 19,
    '#maxlength' => 60, 
  );    

  $form['operations']['subscriber']['status'] = array(
    '#type' => 'select',
    '#options' => array(1 => t('unsubscribed'), 2 => t('subscribed')),
  );
  
  $form['operations']['subscriber']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Add'),
  );  
  // END --------------------------------- Add subscriber

  

  $destination = drupal_get_destination();

  $status = array(1 => t('unsubscribed'), 2 => t('subscribed'));
 
  while ($account = db_fetch_object($result)) {
    $accounts[$account->snid] = '';
    $form['mail'][$account->snid] = array('#value' => $account->mail);
    $form['status'][$account->snid] =  array('#value' => $status[$account->status]);
    
  }
  $form['accounts'] = array(
    '#type' => 'checkboxes',
    '#options' => $accounts
  );
  $form['pager'] = array('#value' => theme('pager', NULL, 50, 0));

  return $form;
}


/**
 * Submit the subscribers update form.
 */
function postsubscribe_subscribers_account_submit($form, &$form_state) {
  
  if ($form_state['values']['subscriber'] != '') {
    $subscriber = $form_state['values']['subscriber'];
    $status = $form_state['values']['status'];
    db_query("insert into {post_subscriptions} (status,mail) values (%d,'%s')", $status, $subscriber);
    drupal_set_message(t('The subscriber has been added.'));

  } 
  else {
    $operations = postsubscribe_subscribers_operations();
    $operation = $operations[$form_state['values']['operation']];
    // Filter out unchecked accounts.
    $accounts = array_filter($form_state['values']['accounts']);
    if ($function = $operation['callback']) {
      // Add in callback arguments if present.
      if (isset($operation['callback arguments'])) {
        $args = array_merge(array($accounts), $operation['callback arguments']);
      }
      else {
        $args = array($accounts);
      }
      call_user_func_array($function, $args);
  
      cache_clear_all('*', 'cache_menu', TRUE);
      drupal_set_message(t('The update has been performed.'));
    }
  }
}

function postsubscribe_subscribers_account_validate($form, &$form_state) { 

  if ($form_state['values']['subscriber'] != '') {
      
      if ( !valid_email_address($form_state['values']['subscriber']) ) {
          form_set_error('', t('Invalid e-mail address.'));        
      }
      
  } 
  else {
    $form_state['values']['accounts'] = array_filter($form_state['values']['accounts']);
    if (count($form_state['values']['accounts']) == 0) {
      form_set_error('', t('No subscriber selected.'));
    }
  }
}


/**
 * 
 */
function postsubscribe_subscribers_operations() {

  $operations = array(
    'unblock' => array(
      'label' => t('Unsubscribe the selected subscriber(s)'),
      'callback' => 'postsubscribe_subscribers_operations_unsubscribe',
    ),
    'block' => array(
      'label' => t('Subscribe the selected subscriber(s)'),
      'callback' => 'postsubscribe_subscribers_operations_subscribe',
    ),
    'delete' => array(
      'label' => t('Delete the selected subscriber(s)'),
     // 'callback' => 'postsubscribe_subscribers_operations_delete',
    ),
  );

  return $operations;
}


/**
 * Callback function for e-mail mass subscribing.
 */
function postsubscribe_subscribers_operations_subscribe($accounts) {   
  foreach ($accounts as $snid) {
    db_query("update {post_subscriptions} set status = %d where snid = %d", 2, $snid);
  }
}

/**
 * Callback function for e-mail mass unsubscribing.
 */
function postsubscribe_subscribers_operations_unsubscribe($accounts) {
  foreach ($accounts as $snid) {
    db_query("update {post_subscriptions} set status = %d where snid = %d", 1, $snid);
  }
}


function postsubscribe_subscribers_multiple_delete_confirm() {
  $edit = $_POST;

  $form['accounts'] = array('#prefix' => '<ul>', '#suffix' => '</ul>', '#tree' => TRUE);
  // array_filter returns only elements with TRUE values
  foreach (array_filter($edit['accounts']) as $snid => $value) {
    $emails = db_result(db_query('SELECT mail FROM {post_subscriptions} WHERE snid = %d', $snid));
    $form['accounts'][$snid] = array('#type' => 'hidden', '#value' => $snid, '#prefix' => '<li>', '#suffix' => check_plain($emails) ."</li>\n");
  }
  $form['operation'] = array('#type' => 'hidden', '#value' => 'delete');

  return confirm_form($form,
                      t('Are you sure you want to delete these subscriber(s)?'),
                      'admin/settings/postsubscribe/subscribers', t('This action cannot be undone.'),
                      t('Delete all'), t('Cancel'));
}

function postsubscribe_subscribers_multiple_delete_confirm_submit($form, &$form_state) {
  if ($form_state['values']['confirm']) {
    foreach ($form_state['values']['accounts'] as $snid => $value) {
      db_query("delete from {post_subscriptions} where snid = %d", $snid);
    }
    drupal_set_message(t('The subscriber(s) have been deleted.'));
  }
   $form_state['redirect'] = 'admin/settings/postsubscribe/subscribers';
}


/**
 * Return form for subscribers filters.
 */
function postsubscribe_subscribers_filter_form() {
  $session = &$_SESSION['postsubscribe_subscribers_overview_filter'];
  $session = is_array($session) ? $session : array();
  $filters = postsubscribe_subscribers_filters();

  $i = 0;
  $form['filters'] = array('#type' => 'fieldset',
                       '#title' => t('Show only subscribers where'),
                       '#theme' => 'postsubscribe_subscribers_filters',
                     );
  foreach ($session as $filter) {
    list($type, $value) = $filter;
    
    if ($type == 'email') {
      $word = 'contains';
      $params = array('%property' => $filters[$type]['title'] , '%value' => $value);
    } 
    else {
      $word = 'is';
      $options = $filters[$type]['options'];
      $params = array('%property' => $filters[$type]['title'] , '%value' => $options[$value]);
    }
    
    if ($i++ > 0) {
      $form['filters']['current'][] = array('#value' => t('<em>and</em> <strong>%property</strong> '. $word .' <strong>%value</strong>', $params));
    }
    else {
      $form['filters']['current'][] = array('#value' => t('<strong>%property</strong> '. $word .' <strong>%value</strong>', $params));
    }
    $i++;
  }

  foreach ($filters as $key => $filter) {
    $names[$key] = $filter['title'];
    if ($key == 'email') {
     $form['filters']['status'][$key] = array('#type' => 'textfield', 
                                              '#default_value' => '',
                                              '#size' => 19,
                                              '#maxlength' => 60, 
                                       );    
    } 
    else {
     $form['filters']['status'][$key] = array('#type' => 'select',
                                         '#options' => $filter['options'],
                                       );
    }
  }

  $form['filters']['filter'] = array('#type' => 'radios',
                                 '#options' => $names,
                               );
  $form['filters']['buttons']['submit'] = array('#type' => 'submit',
                                            '#value' => (count($session) ? t('Refine') : t('Filter'))
                                          );
  if (count($session)) {
    $form['filters']['buttons']['undo'] = array('#type' => 'submit',
                                            '#value' => t('Undo')
                                          );
    $form['filters']['buttons']['reset'] = array('#type' => 'submit',
                                             '#value' => t('Reset')
                                           );
  }

  return $form;
}


/**
 * Theme user administration filter form.
 */
function theme_postsubscribe_subscribers_filter_form($form) {
  $output = '<div id="user-admin-filter">';
  $output .= drupal_render($form['filters']);
  $output .= '</div>';
  $output .= drupal_render($form);
  return $output;
}

/**
 * Theme subscribers filter selector.
 */
function theme_postsubscribe_subscribers_filters($form) {

  $output = '<ul class="clear-block">';
  if (sizeof($form['current'])) {
    foreach (element_children($form['current']) as $key) {
      $output .= '<li>'. drupal_render($form['current'][$key]) .'</li>';
    }
  }

  $output .= '<li><dl class="multiselect">'. (sizeof($form['current']) ? '<dt><em>'. t('and') .'</em> '. t('where') .'</dt>' : '') .'<dd class="a">';
  foreach (element_children($form['filter']) as $key) {
    $output .= drupal_render($form['filter'][$key]);
  }
  $output .= '</dd>';

  $output .= '<dt>'. t('is') .'</dt><dd class="b">';

  foreach (element_children($form['status']) as $key) {
   // print_r($form['status'][$key]);
    $output .= drupal_render($form['status'][$key]);
  }
  $output .= '</dd>';

  $output .= '</dl>';
  $output .= '<div class="container-inline" id="user-admin-buttons">'. drupal_render($form['buttons']) .'</div>';
  $output .= '</li></ul>';

  return $output;
}


/**
 * Process result from subscribers filter form.
 */
function postsubscribe_subscribers_filter_form_submit($form, &$form_state) {
  $op = $form_state['values']['op'];
  $filters = postsubscribe_subscribers_filters();
  switch ($op) {
    case t('Filter'): case t('Refine'):
      if (isset($form_state['values']['filter'])) {
        $filter = $form_state['values']['filter'];
        
        if ($filter == 'email') {
            $_SESSION['postsubscribe_subscribers_overview_filter'][] = array($filter, $form_state['values'][$filter]);
        } 
        else {
          $options = $filters[$filter]['options'];
          if (isset($options[$form_state['values'][$filter]])) {
            $_SESSION['postsubscribe_subscribers_overview_filter'][] = array($filter, $form_state['values'][$filter]);
          }
        }
      
      }
      break;
    case t('Undo'):
      array_pop($_SESSION['postsubscribe_subscribers_overview_filter']);
      break;
    case t('Reset'):
      $_SESSION['postsubscribe_subscribers_overview_filter'] = array();
      break;
    case t('Update'):
      return;
  }

  $form_state['redirect'] = 'admin/settings/postsubscribe/subscribers';
}


/**
 * List subscribers filters that can be applied.
 */
function postsubscribe_subscribers_filters() {
  // Regular filters
  $filters = array();
                
  
  $filters['status'] = array('title' => t('status'),
                         'where' => 's.status = %d',
                         'options' => array(1 => t('unsubscribed'), 2 => t('subscribed')),
                       );
  $filters['email'] = array('title' => t('email'),
                         'where' => " s.mail LIKE '%%%s%%'",
                         'join' => '',
                       );

  return $filters;
}

