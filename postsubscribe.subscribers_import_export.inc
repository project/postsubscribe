<?php
/*
 *  @file
 *  Copyright (C) The World Bank. 2008. http://www.worldbank.org
 *	Author: Lasha Dolidze
 *
 */


/**
 *  Import Page Screen
 */
function postsubscribe_subscribers_import() {
  return drupal_get_form('postsubscribe_subscribers_import_form');
}

/**
 * Form to import Subscribers.
 *
 */
function postsubscribe_subscribers_import_form(&$form_state) {

  $form = array();
  $form['import'] = array(
    '#type' => 'fieldset',
    '#title' => t('Import Subscribers'),
    '#collapsible' => TRUE,
  );
  $form['import']['file'] = array(
    '#type' => 'file',
    '#title' => t('Subscribers file'),
    '#size' => 50,
    '#description' => t('A (<em>.csv</em>) file.'),
  );
  $form['import']['mode'] = array(
    '#type' => 'radios',
    '#title' => t('Mode'),
    '#default_value' => 2,
    '#options' => array(
      1 => t('Subscribers in the uploaded file replace existing ones, new ones are added'),
      2 => t('Existing Subscribers are kept, only new Subscribers are added')
    ),
  );
  
  $form['import']['submit'] = array('#type' => 'submit', '#value' => t('Import'));
  $form['#attributes']['enctype'] = 'multipart/form-data';

  return $form;

}


/**
 *  Export Page Screen
 */
function postsubscribe_subscribers_export() {
  return drupal_get_form('postsubscribe_subscribers_export_form');
}


/**
 * Form to export Subscribers.
 *
 */
function postsubscribe_subscribers_export_form(&$form_state) {

  $form['export'] = array(
    '#type' => 'fieldset',
    '#title' => t('Export Subscribers'),
    '#collapsible' => TRUE,
  );
  
  $form['export']['status'] = array(
    '#type' => 'radios',
    '#title' => t('Status'),
    '#default_value' => 0,
    '#options' => array(0 => t('All'), 1 => t('Unsubscribed'), 2 => t('Subscribed')),
    '#description' => t('Select the status to export.'),
  );
  
  $form['export']['submit'] = array('#type' => 'submit', '#value' => t('Export'));
  $form['#submit'][] = 'postsubscribe_subscribers_export_form_submit';
  
  return $form;
}



/**
 * Process the Subscribers import form submission.
 */
function postsubscribe_subscribers_import_form_submit($form, &$form_state) {
  // Ensure we have the file uploaded
  if ($file = file_save_upload('file')) {
    $mode = $form_state['values']['mode'];
    $subscribers = _postsubscribe_subscribers_import_read_csv($file);
    foreach ($subscribers as $key => $value) {
      $subscriber = $value[0];
      $status = $value[1];
      
      if ($mode == 2) {
        if (!db_fetch_object(db_query("SELECT mail FROM {post_subscriptions} WHERE mail = '%s'", $subscriber))) {
          db_query("insert into {post_subscriptions} (status,mail) values (%d,'%s')", $status, $subscriber);
        }
      }
      else if ($mode == 1) {
        if (db_fetch_object(db_query("SELECT mail FROM {post_subscriptions} WHERE mail = '%s'", $subscriber))) {
            db_query("update {post_subscriptions} set status = %d where mail = '%s'", $status, $subscriber);
        }
        else {
            db_query("insert into {post_subscriptions} (status,mail) values (%d,'%s')", $status, $subscriber);
        }     
      }
    }
    
    drupal_set_message(t('The subscriber has been imported.'));

    //die();
  }
  else {
    drupal_set_message(t('File to import not found.'), 'error');
    $form_state['redirect'] = 'admin/settings/postsubscribe/subscribers/import';
    return;
  }

  $form_state['redirect'] = 'admin/settings/postsubscribe/subscribers/import';
  return;
}

/**
 * Parses CSV file into an array
 *
 * @param $file
 *   Drupal file object corresponding to the PO file to import
 */
function _postsubscribe_subscribers_import_read_csv($file) {
  
  $fd = fopen($file->filepath, "rb"); // File will get closed by PHP on return
  if (!$fd) {
    _postsubscribe_subscribers_import_message('The Subscribers import failed, because the file %filename could not be read.', $file);
    fclose($fd);
    return FALSE;
  }
  
  $lineno = -1;
  $allowed_status = array("unsubscribed", "subscribed", "1", "2");
  $subscribers = array();
  
  while (($data = fgetcsv($fd, 1000, ",")) !== FALSE) {
      $lineno++;
      $num = count($data);
      if ($lineno == 0) { 
        if ($num != 2 || strtolower($data[0]) != 'email' || strtolower($data[1]) != 'status' ) {
           _postsubscribe_subscribers_import_message('The Subscribers import failed, because the file %filename is invalid.', $file);
           fclose($fd);
           return FALSE;
        }
        continue;    
      }
      $email = trim($data[0]);
      $status = trim($data[1]);
      
      // Check email address
      if ( !valid_email_address($email) ) {
          _postsubscribe_subscribers_import_message('Invalid email address ('. check_plain($email) .') on row %line (Skip this subscriber).', NULL, $lineno);
          continue;        
      }
      
      // Check status
      if (!in_array($status, $allowed_status)) {
          _postsubscribe_subscribers_import_message('Invalid subscriber status ('. check_plain($status) .') on row %line (Skip this subscriber).', NULL, $lineno);
          continue;         
      }
      
      if ($status == 'unsubscribed') {
        $status = 1;
      }
      else if ($status == 'subscribed') {
        $status = 2;
      }
      
      $subscribers[] = array($email, $status);
  }
  
  fclose($fd);
  
  return $subscribers;
}


/**
 * Sets an error message occurred during subscribers file parsing.
 *
 * @param $message
 *   The message to be translated
 * @param $file
 *   Drupal file object corresponding to the CSV file to import
 * @param $lineno
 *   An optional line number argument
 */
function _postsubscribe_subscribers_import_message($message, $file, $lineno = NULL) {
  $vars = array('%filename' => $file->filename);
  if (isset($lineno)) {
    $vars['%line'] = $lineno;
  }
  $t = get_t();
  
  drupal_set_message($t($message, $vars), 'error');
}


/**
 * Process a subscribers export form submission.
 */
function postsubscribe_subscribers_export_form_submit($form, &$form_state) {
  
  // By default we export all subscribers, 
  // with status 1 and 2 ( 1 -> Unsubscribed, 2 -> Subscribed ).
  $status = '1,2';
  if (isset($form_state['values']['status']) && $form_state['values']['status'] != 0) {
      $status = $form_state['values']['status'];
  }

  postsubscribe_subscribers_export_csv($status);
}


/**
 * Export to CSV.
 */
function postsubscribe_subscribers_export_csv($status) { 
  
  $sql = 'SELECT s.mail, s.status FROM {post_subscriptions} s where s.status in ('. db_escape_string($status) .')';
  $result = db_query($sql);
        
  $csv_output = "\"Email\",\"Status\""; 
  $csv_output .= "\r\n"; 
  
  while ($row = db_fetch_object($result)) {
    $temail = '"'. $row->mail .'"';
    $tstatus = '"'. $row->status .'"';
    $csv_output .= "$temail,$tstatus\r\n";
  }
  
  header("Content-Disposition: attachment; filename=Subscribers_". date("Y-m-d") .".csv");  
  header("Content-Type: application/vnd.ms-excel");
  print $csv_output;
  die();
} 

